% PARAMETERS:

	Protocol:			 ashraf
	Problem category:		 if

	IF2SATE Version:		 2.0
	Compound types:			 off
	Step compression:		 on
	Intruder Knowledge As Axioms:	 off
	Weak Type-Flaws (iff newgp):	 off

	Technique:			 Graphplan-based Encoding using the EFA schema
	Min Steps:			 1
	Max Steps:			 30
	Delta Steps:			 1
	Level Mutex:			 1
	Solver:				 zchaff
